import 'package:flutter/material.dart';

class BmiHome extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new BMIState();
  }
}

class BMIState extends State<BmiHome> {
  double _calc;
  String _bmiResult;
  String _status;
  TextEditingController _ageFieldController = new TextEditingController();
  TextEditingController _heightFieldController = new TextEditingController();
  TextEditingController _weightFieldController = new TextEditingController();

  void _calculateBMI() {
    double height = double.parse(_heightFieldController.text);
    double weight = double.parse(_weightFieldController.text);

    if (height != null && weight != null) {
      setState(() {
        height = height/100;
        _calc = (weight / (height * height));
        _bmiResult = _calc.toStringAsFixed(2);
        print('height: $height');
        print('weight: $weight');
        print('_calc: $_calc');
        print('_bmiResult: $_bmiResult');
        if (_calc < 15.0) {
          _status = 'Delgadez muy severa';
        } else if (_calc >= 15.0 && _calc < 15.9) {
          _status = 'delgadez severa';
        } else if (_calc >= 15.9 && _calc < 18.4) {
          _status = 'Delgadez';
        } else if (_calc >= 18.4 && _calc < 24.9) {
          _status = 'Peso saludable';
        } else if (_calc >= 24.9 && _calc < 29.9 ){
          _status = 'Sobrepeso';
        } else if (_calc >= 29.9 && _calc < 34.9){
          _status = 'Obesidad moderada';
        } else if (_calc >= 34.9 && _calc < 39.9){
          _status = 'Obesidad severa';
        } else if (_calc >=40){
          _status = 'Obsidad muy severa';
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.pink,
        title: new Text('BMI'),
      ),
      body: new ListView(
        padding: const EdgeInsets.all(2.5),
        children: <Widget>[
          new Image.asset(
            "images/bmilogo.png",
            height: 120.0,
            width: 150.0,
          ),
          new Container(
            color: Colors.grey.shade300,
            padding: const EdgeInsets.all(5.0),
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new TextField(
                    controller: _ageFieldController,
                    decoration: new InputDecoration(
                        labelText: 'Age', icon: new Icon(Icons.person))),
                new TextField(
                  controller: _heightFieldController,
                  decoration: new InputDecoration(
                      labelText: 'Height in cm',
                      icon: new Icon(Icons.linear_scale)),
                ),
                new TextField(
                  controller: _weightFieldController,
                  decoration: new InputDecoration(
                      labelText: 'Weight in kg',
                      icon: new Icon(Icons.arrow_upward)),
                ),
                new Padding(
                  padding: const EdgeInsets.all(5.5),
                ),
                new RaisedButton(
                  onPressed: _calculateBMI,
                  child: new Text(
                    'CALCULATE',
                    style: new TextStyle(fontWeight: FontWeight.bold),
                  ),
                  color: Colors.pink,
                  textColor: Colors.white,
                )
              ],
            ),
          ),
          new Padding(padding: const EdgeInsets.all(5.5)),
          new Container(
            alignment: Alignment.topCenter,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text(_bmiResult != null ? 'BMI: $_bmiResult' : 'Enter your stats above',
                  style: new TextStyle(
                    color: Colors.green,
                    fontSize: 24.5,
                  ),
                ),
                new Text(_status != null ? _status : '',
                  style: new TextStyle(
                    fontSize: 28.0
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
