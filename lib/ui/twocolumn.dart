import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

class TwoColumn extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new Screen();
  }
}

class Screen extends State<TwoColumn> {

  double _resultado;
  TextEditingController _p1 = new TextEditingController();
  TextEditingController _p2 = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.blue,
        title: new Text('Varios campos de texto'),
      ),
      body: new Container(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            buildAppEntryRow(context),
            buildAppArea(context),
            buildAppButtonCalculate(context),
          ],
        ),
      ),
    );
  }

  Widget buildAppEntryRow(BuildContext context) {
    return Row(
      children: <Widget>[
        Flexible(
          child: TextField(
            controller: _p1,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular((4))),
                borderSide: BorderSide(width: 1)
              ),
              hintText: 'Base',
              icon: Icon(MdiIcons.alphaXCircleOutline, size: 50,),
            ),
          ),
        ),
        Flexible(
          child: TextField(
            controller: _p2,
            decoration: InputDecoration(
              hintText: 'Altura',
              icon: Icon(MdiIcons.alphaYCircleOutline, size: 50),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular((4))),
                borderSide: BorderSide(width: 1)
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget buildAppArea(BuildContext context){
    return Padding(
        padding: const EdgeInsets.all(30),
        child: Text(
      _resultado.toString(),
          style: TextStyle(fontSize: 20),
    )
    );
  }

  void areaTrianguloRectangulo(TextEditingController b, TextEditingController h){
    setState(() {
      double B = double.parse(b.text);
      double H = double.parse(h.text);
      _resultado = (B*H)/2;
    });
  }

  @override
  void initState(){
    super.initState();
    _p1.text = '';
    _p2.text = '';
    _resultado = 0;
  }

  Widget buildAppButtonCalculate(BuildContext context){
    return RaisedButton.icon(onPressed: (){areaTrianguloRectangulo(_p1, _p2);}, icon: Icon(MdiIcons.triangleOutline, color: Colors.lightGreenAccent,), label: Text('área'), color: Colors.lightBlue,);
  }



}
